# `mac-up`

> This repository contains some scripts and config files to get a new MacBook Pro into the state I prefer using.

For the time being, I'll start with a couple of bash scripts and configuration files. Maybe later, if it makes sense, I'll check out how I could use SaltStack or some other scripting language instead of bash. No Idea. We will figure it out.
